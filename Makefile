default: build dockerize

build:
	mvn clean package -U -Dmaven.test.skip=true

dockerize:
	docker build -f conferencebenchmarkcontroller.docker -t git.project-hobbit.eu:4567/ondrej.zamazal/conference .
	docker build -f conferencedatagenerator.docker -t git.project-hobbit.eu:4567/ondrej.zamazal/conferencedatagenerator .
	docker build -f conferencetaskgenerator.docker -t git.project-hobbit.eu:4567/ondrej.zamazal/conferencetaskgenerator .
	docker build -f conferenceevaluationmodule.docker -t git.project-hobbit.eu:4567/ondrej.zamazal/conferenceevaluationmodule .

	docker push git.project-hobbit.eu:4567/ondrej.zamazal/conference
	docker push git.project-hobbit.eu:4567/ondrej.zamazal/conferencedatagenerator
	docker push git.project-hobbit.eu:4567/ondrej.zamazal/conferencetaskgenerator
	docker push git.project-hobbit.eu:4567/ondrej.zamazal/conferenceevaluationmodule
