package org.hobbit.conferencebenchmark;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
//import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringBufferInputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDF;
import org.hobbit.core.Constants;
import org.hobbit.core.components.AbstractEvaluationModule;
import org.hobbit.core.rabbit.RabbitMQUtils;
import org.hobbit.conferencebenchmark.alignment.HashAlignment;
import org.hobbit.conferencebenchmark.alignment.RDFAlignReader;
import org.hobbit.conferencebenchmark.util.PlatformConstants;
import org.hobbit.vocab.HOBBIT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;




//Google Drive
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.media.MediaHttpUploader;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpTransport;
//import com.google.api.client.http.*;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.DataStoreFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;

import cz.vse.swoe.test.Gapi.View;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;

//import com.dropbox.core.DbxRequestConfig;
//import com.dropbox.core.v2.DbxClientV2;
//import com.dropbox.core.v2.files.FileMetadata;
//import com.dropbox.core.v2.users.FullAccount;

/**
 * Re-used from the LargeBio track
 *
 * @author ernesto
 * @author ondrej
 * 
 * Modifications by ondrej on 15 Jan 2018
 *
 */


/**
 * Based on the SpatialBenchmark eval module, but changed input alignment format
 *
 * @author ernesto
 * Created on 11 Jan 2018
 *
 */

public class EvaluationModule extends AbstractEvaluationModule {
	
	//Google Drive
	private static final String APPLICATION_NAME = "Conference";

	  private static final String UPLOAD_FILE_PATH = PlatformConstants.datasetsPath + java.io.File.separator + "reference-cmt-conference.rdf";
	  private static final java.io.File UPLOAD_FILE = new java.io.File(UPLOAD_FILE_PATH);

	  /** Directory to store user credentials. */
	  private static final java.io.File DATA_STORE_DIR =
	      new java.io.File(System.getProperty("user.home"), ".store/drive_sample");

	  /**
	   * Global instance of the {@link DataStoreFactory}. The best practice is to make it a single
	   * globally shared instance across your application.
	   */
	  private static FileDataStoreFactory dataStoreFactory;

	  /** Global instance of the HTTP transport. */
	  private static HttpTransport httpTransport;

	  /** Global instance of the JSON factory. */
	  private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

	  /** Global Drive API client. */
	  private static Drive drive;
	//end Google Drive
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EvaluationModule.class);

    private Property EVALUATION_RECALL = null;
    private Property EVALUATION_PRECISION = null;
    private Property EVALUATION_FMEASURE = null;
    private Property EVALUATION_TIME_PERFORMANCE = null;

    private Model finalModel = ModelFactory.createDefaultModel();

    private int truePositives = 0;
    private int falsePositives = 0;
    private int falseNegatives = 0;

    //private double time_perfomance = 0;
    public long time_performance = 0;
    
    //private DbxClientV2 client;
    
    //Google Drive method authorize
    /** Authorizes the installed application to access user's protected data. */
    private static Credential authorize() throws Exception {
      // load client secrets
      GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY,
          new InputStreamReader(EvaluationModule.class.getResourceAsStream("/client_secrets.json")));
      if (clientSecrets.getDetails().getClientId().startsWith("Enter")
          || clientSecrets.getDetails().getClientSecret().startsWith("Enter ")) {
        System.out.println(
            "Enter Client ID and Secret from https://code.google.com/apis/console/?api=drive "
            + "into drive-cmdline-sample/src/main/resources/client_secrets.json");
        System.exit(1);
      }
      // set up authorization code flow
      GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
          httpTransport, JSON_FACTORY, clientSecrets,
          Collections.singleton(DriveScopes.DRIVE_FILE)).setDataStoreFactory(dataStoreFactory)
          .build();
      // authorize
      return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
    }
    //end Google Drive method authorize
    
    //Google Drive method upload
    /** Uploads a file using either resumable or direct media upload. */
    private static File uploadFile(boolean useDirectUpload) throws IOException {
      File fileMetadata = new File();
      fileMetadata.setTitle(UPLOAD_FILE.getName());

      //FileContent mediaContent = new FileContent("image/jpeg", UPLOAD_FILE);
      FileContent mediaContent = new FileContent("text/plain", UPLOAD_FILE);

      Drive.Files.Insert insert = drive.files().insert(fileMetadata, mediaContent);
      //Drive.Files.Insert insert = drive.files().insert(fileMetadata);
      
      MediaHttpUploader uploader = insert.getMediaHttpUploader();
      uploader.setDirectUploadEnabled(useDirectUpload);
      //uploader.setProgressListener(new FileUploadProgressListener());
      return insert.execute();
    }
    //end Google Drive method upload
    
    @Override
    public void init() throws Exception {
        LOGGER.info("Initializing Evaluation Module started...");
        super.init();

        Map<String, String> env = System.getenv();

        /* time performance */
        if (!env.containsKey(PlatformConstants.EVALUATION_TIME_PERFORMANCE)) {
            throw new IllegalArgumentException("Couldn't get \"" + PlatformConstants.EVALUATION_TIME_PERFORMANCE
                    + "\" from the environment. Aborting.");
        }
        EVALUATION_TIME_PERFORMANCE = this.finalModel
                .createProperty(env.get(PlatformConstants.EVALUATION_TIME_PERFORMANCE));

        LOGGER.info("EVALUATION_TIME_PERFORMANCE setted");
        /* recall */
        if (!env.containsKey(PlatformConstants.EVALUATION_RECALL)) {
            throw new IllegalArgumentException("Couldn't get \"" + PlatformConstants.EVALUATION_RECALL
                    + "\" from the environment. Aborting.");
        }
        EVALUATION_RECALL = this.finalModel
                .createProperty(env.get(PlatformConstants.EVALUATION_RECALL));

        LOGGER.info("EVALUATION_RECALL setted");

        /* precision */
        if (!env.containsKey(PlatformConstants.EVALUATION_PRECISION)) {
            throw new IllegalArgumentException("Couldn't get \""
                    + PlatformConstants.EVALUATION_PRECISION + "\" from the environment. Aborting.");
        }
        EVALUATION_PRECISION = this.finalModel
                .createProperty(env.get(PlatformConstants.EVALUATION_PRECISION));

        LOGGER.info("EVALUATION_PRECISION setted");

        /* fmeasure */
        if (!env.containsKey(PlatformConstants.EVALUATION_FMEASURE)) {
            throw new IllegalArgumentException("Couldn't get \"" + PlatformConstants.EVALUATION_FMEASURE
                    + "\" from the environment. Aborting.");
        }
        EVALUATION_FMEASURE = this.finalModel
                .createProperty(env.get(PlatformConstants.EVALUATION_FMEASURE));

        LOGGER.info("EVALUATION_FMEASURE setted");

        LOGGER.info("Initializing Evaluation Module ended...");
        
        LOGGER.info("Initializing DropBox...");
        
        InputStream in = new FileInputStream("pk.txt");
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		String key=reader.readLine();
        
        // Create Dropbox client
        //DbxRequestConfig config = new DbxRequestConfig("dropbox/java-tutorial", "en_US");
        //DbxRequestConfig config = new DbxRequestConfig("OAEI");
        //DbxClientV2 client = new DbxClientV2(config, key);
        
    }


	@Override
	protected void evaluateResponse(byte[] expectedData, byte[] receivedData, long taskSentTimestamp,
			long responseReceivedTimestamp) throws Exception {
		
		
		//TODO 
		//ExpectedData and ReceivedData contain the ByteArrays of the OAEI mappings in RDF Alignment format 
		//1. Use ByteArrayInputStream to read OAEI files: see RDFAlignReader
		//2. Get mappings as HashAlignment
		//3. Calculate metrics
    	
    	
    	
        time_performance = responseReceivedTimestamp - taskSentTimestamp;
        if (time_performance < 0) {
            time_performance = 0;
        }
        LOGGER.info("time_performance in ms: " + time_performance);


        LOGGER.info("Read expected data");
        ByteBuffer buffer_exp = ByteBuffer.wrap(expectedData);
        
        //Although not needed we need to read them the buffer in the required order 
        String format = RabbitMQUtils.readString(buffer_exp);
        String path = RabbitMQUtils.readString(buffer_exp);
        byte[] expected = RabbitMQUtils.readByteArray(buffer_exp);
        
        LOGGER.info("Read system data");
        ByteBuffer buffer_sys = ByteBuffer.wrap(receivedData);
        
      //Although not needed we need to read them the buffer in the required order        
        byte[] received = RabbitMQUtils.readByteArray(buffer_sys);
        
        
        
        //Read alignments 
        HashAlignment ref_alignment;
        HashAlignment system_alignment;
                
        //Reference alignments
        if (expected.length > 0) {
        	//read file
        	RDFAlignReader reader = new RDFAlignReader(expected);
        	ref_alignment = reader.getHashAlignment();
        }
        else{
        	ref_alignment = new HashAlignment();
        }        
        LOGGER.info("Conference reference alignment size: " + ref_alignment.size());
        
        
        
        //System
        LOGGER.info("Read system data");        
        if (received.length > 0) {        	
        	//read file
        	RDFAlignReader reader = new RDFAlignReader(received);
        	system_alignment = reader.getHashAlignment();
        	//store to dropbox the alignment in RDF format
        	//OutputStream stream = new FileOutputStream("x1.owl");
        	//stream.write(received);
        	//stream.close();        	                	
        	
        	//FileMetadata metadata = client.files().uploadBuilder("/test.txt").uploadAndFinish(new ByteArrayInputStream(received));
        	// Upload "test.txt" to Dropbox
            /*
        	try {
            	// Get current account info
            	//FullAccount account = client.users().getCurrentAccount();
            	//LOGGER.info("dropbox says 0:"+account.getName().getDisplayName());
            	
	        	LOGGER.info("Uploading to dropbox...");
	            //FileMetadata metadata = client.files().uploadBuilder("/test5.txt").	        	
	            		//uploadAndFinish(new StringBufferInputStream("It works."));
        	}
        	catch(Exception e) {
        		LOGGER.info("dropbox says 1:"+e.getCause());
        		LOGGER.info("dropbox says 2:"+e.getMessage());
        		LOGGER.info("dropbox error:"+e);
        		System.err.println(e);
        		e.printStackTrace();
        	}
        	*/
        	/*
        	try (InputStream in = new FileInputStream(PlatformConstants.datasetsPath + File.separator + "reference-cmt-conference.rdf")) {
        	    FileMetadata metadata = client.files().uploadBuilder("/reference-cmt-conference.rdf")
        	        .uploadAndFinish(in);
        	}
        	catch(Exception e) {
        		LOGGER.info("dropbox says 1:"+e.getCause());
        		LOGGER.info("dropbox says 2:"+e.getMessage());
        		LOGGER.info("dropbox error:"+e);
        		System.err.println(e);
        		e.printStackTrace();
        	}
        	*/
        	//FileMetadata metadata = client.files().uploadBuilder("/test.txt").uploadAndFinish(in);
            
        	//store to dropbox the alignment in RDF format
        	
        	//Google Drive
        	try {
        	      httpTransport = GoogleNetHttpTransport.newTrustedTransport();
        	      dataStoreFactory = new FileDataStoreFactory(DATA_STORE_DIR);
        	      // authorization
        	      Credential credential = authorize();
        	      // set up the global Drive instance
        	      drive = new Drive.Builder(httpTransport, JSON_FACTORY, credential).setApplicationName(
        	          APPLICATION_NAME).build();

        	      // run commands

        	      LOGGER.info("Starting Simple Media Upload");
        	      File uploadedFile = uploadFile(true);
        	      LOGGER.info("Google Drive Success!");
        	      return;
        	    } catch (IOException e) {
        	      System.err.println(e.getMessage());
        	    } catch (Throwable t) {
        	      t.printStackTrace();
        	    }
        	//end Google Drive
        	
        }
        else{
        	system_alignment = new HashAlignment();
        }
        
        
        LOGGER.info("Conference system alignment size: " + system_alignment.size());
        LOGGER.info("printing system alignment");
    	printMapping(system_alignment, system_alignment.size(), "system correspondence");
       
        
        
        //Calculate metrics
        int[] results = ref_alignment.evaluation(system_alignment);
        
        
        truePositives = results[0];
	    falsePositives = results[1];
	    falseNegatives = results[2];
	    
	    
	    LOGGER.info("COMPARISON-> TP  FP  FN:" + truePositives + " " + falsePositives + " " + falseNegatives);
		
	}
	
	@Override
    public Model summarizeEvaluation() throws Exception {
        LOGGER.info("Summary of Evaluation begins.");

        if (this.experimentUri == null) {
            Map<String, String> env = System.getenv();
            this.experimentUri = env.get(Constants.HOBBIT_EXPERIMENT_URI_KEY);
        }

        double recall = 0.0;
        double precision = 0.0;
        double fmeasure = 0.0;

        if ((double) (this.truePositives + this.falseNegatives) > 0.0) {
            recall = (double) this.truePositives / (double) (this.truePositives + this.falseNegatives);
        }
        if ((double) (this.truePositives + this.falsePositives) > 0.0) {
            precision = (double) this.truePositives / (double) (this.truePositives + this.falsePositives);
        }
        if ((double) (recall + precision) > 0.0) {
            fmeasure = (double) (2.0 * recall * precision) / (double) (recall + precision);
        }

        //////////////////////////////////////////////////////////////////////////////////////////////
        Resource experiment = this.finalModel.createResource(experimentUri);
        this.finalModel.add(experiment, RDF.type, HOBBIT.Experiment);

        Literal timePerformanceLiteral = this.finalModel.createTypedLiteral(this.time_performance, XSDDatatype.XSDlong);
        this.finalModel.add(experiment, this.EVALUATION_TIME_PERFORMANCE, timePerformanceLiteral);

        Literal recallLiteral = this.finalModel.createTypedLiteral(recall,
                XSDDatatype.XSDdouble);
        this.finalModel.add(experiment, this.EVALUATION_RECALL, recallLiteral);

        Literal precisionLiteral = this.finalModel.createTypedLiteral(precision,
                XSDDatatype.XSDdouble);
        this.finalModel.add(experiment, this.EVALUATION_PRECISION, precisionLiteral);

        Literal fmeasureLiteral = this.finalModel.createTypedLiteral(fmeasure,
                XSDDatatype.XSDdouble);
        this.finalModel.add(experiment, this.EVALUATION_FMEASURE, fmeasureLiteral);

        LOGGER.info("Summary of Evaluation is over.");

        return this.finalModel;
    }
	
	
	/**
	 * For testing only
	 * @param max_number
	 */
	private void printMapping(HashAlignment alignment, int max_number, String origin){
		 
        int i=0;
        for (String s : alignment.getSources()){
        	for (String t : alignment.getTargets(s)){
        		LOGGER.info(origin + ": " + s + " " + t);        		
        		if (++i>max_number) return;
        	}
        	 
        }
	}

	
}
