package org.hobbit.conferencebenchmark;

/**
 * Re-used from the LargeBio track
 *
 * @author ernesto
 *
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.hobbit.core.Commands;
import org.hobbit.core.components.AbstractTaskGenerator;
import org.hobbit.core.rabbit.RabbitMQUtils;
import org.hobbit.core.rabbit.SimpleFileReceiver;
import org.hobbit.core.rabbit.SimpleFileSender;
import org.hobbit.conferencebenchmark.task.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TaskGenerator extends AbstractTaskGenerator {
	 private static final Logger LOGGER = LoggerFactory.getLogger(TaskGenerator.class);
	    private SimpleFileReceiver inputFilesReceiver;

	    public TaskGenerator() {
	        super(1);
	}
	
 @Override
 public void init() throws Exception {

     LOGGER.info("Initializing Task Generators...");
     super.init();
     //Sent from Data Generator
     inputFilesReceiver = SimpleFileReceiver.create(this.incomingDataQueueFactory, "input_files");
     LOGGER.info("Task Generators initialized successfully.");
 }

	
	@Override
	protected void generateTask(byte[] data) throws Exception {
		try {
			
			
			//Receive files from DataGenerator: source and target ontologies
			//String[] receivedFiles = inputFilesReceiver.receiveData("./datasets/");
			
			//File file_source = new File("./datasets/" + receivedFiles[0]);
			//File file_target = new File("./datasets/" + receivedFiles[1]); 
			 
			
         //============== We Process inforamtion about the task and send it to System Adaptor and Evaluation Storage ==============
         
         // Create an ID for the task
         Task task = (Task) SerializationUtils.deserialize(data);
         String taskId = task.getTaskId();
         
         
         boolean isMatchingClassesRequired = task.isMatchingClassesRequired();
         boolean isMatchingDataPropertiesRequired = task.isMatchingDataPropertiesRequired();
         boolean isMatchingObjectPropertiesRequired = task.isMatchingObjectPropertiesRequired();
         boolean isMatchingInstancesRequired = task.isMatchingInstancesRequired();
         
         
         byte[] source_target = task.getSourceTarget();
         ByteBuffer taskBuffer = ByteBuffer.wrap(source_target);
         //We read 3 parameters
         String format = RabbitMQUtils.readString(taskBuffer);
         String path_source = RabbitMQUtils.readString(taskBuffer);
         String path_target = RabbitMQUtils.readString(taskBuffer);
         
		path_source = path_source.substring(path_source.lastIndexOf("/")+1);
		path_target = path_target.substring(path_target.lastIndexOf("/")+1);
		
		LOGGER.info("Path to files source "+path_source+" target "+path_target);
         
        //============== We sent TASK files to System ==============
     	// We will send source and target files to System using sender
		//Define a queue name to be identifiable by system
        //Name of the queue to send files to system adapter. We give a "proper" name, since in multiple-task benchmarks there should be a way to identify them
        String queueName = task.getTaskQueueName();	        
		 
		File file_source = new File("./datasets/" + path_source);
		File file_target = new File("./datasets/" + path_target);
		LOGGER.info("Source "+file_source+" and target "+file_target+ "files succesfully received from data generator");
		LOGGER.info("Source "+file_source.getName()+" and target "+file_target.getName()+ " files succesfully received from data generator");
		
		// create the sender
		SimpleFileSender sender = SimpleFileSender.create(this.outgoingDataQueuefactory, queueName);
		
		InputStream is_source = null;
		InputStream is_target = null;
		try {
		    // create input stream, e.g., by opening a file
		    is_source = new FileInputStream(file_source);
		    is_target = new FileInputStream(file_target);
		    // send data
		    sender.streamData(is_source, file_source.getName());
		    sender.streamData(is_target, file_target.getName());
		} catch (Exception e) {
		    // handle exception
		} finally {
		    IOUtils.closeQuietly(is_source);
		    IOUtils.closeQuietly(is_target);
		}
		// close the sender
		IOUtils.closeQuietly(sender);
		LOGGER.info("Source "+file_source.getName()+" and target "+file_target.getName()+" files succesfully send as task_files by sender");
         
         //===================== SYSTEM will receive these 8 parameters
         //This order will be important for the System adapter
         byte[][] taskDataArray = new byte[8][];
         //The first two seems to be "standard" across HOBBIT benchmarks
         //FOR OAEI it is also important to provide both source and target associated to the task
         taskDataArray[0] = RabbitMQUtils.writeString(format);
         //Important to send the name
         taskDataArray[1] = RabbitMQUtils.writeString(file_source.getName());
         taskDataArray[2] = RabbitMQUtils.writeString(file_target.getName());
         //For OAEI is a desired characteristic to tell what to match
         taskDataArray[3] = RabbitMQUtils.writeString(String.valueOf(isMatchingClassesRequired));
         taskDataArray[4] = RabbitMQUtils.writeString(String.valueOf(isMatchingDataPropertiesRequired));
         taskDataArray[5] = RabbitMQUtils.writeString(String.valueOf(isMatchingObjectPropertiesRequired));
         taskDataArray[6] = RabbitMQUtils.writeString(String.valueOf(isMatchingInstancesRequired));

         //Task name and queueID for system
         taskDataArray[7] = RabbitMQUtils.writeString(task.getTaskQueueName());
         
         byte[] taskData = RabbitMQUtils.writeByteArrays(taskDataArray);

         byte[] expectedAnswerData = task.getExpectedAnswers();

         // Send the task to the system (and store the timestamp)
         
         sendTaskToSystemAdapter(taskId, taskData);
         LOGGER.info("Task " + taskId + " sent to System Adapter.");

         // Send the expected answer to the evaluation store
         long timestamp = System.currentTimeMillis();
         sendTaskToEvalStorage(taskId, timestamp, expectedAnswerData);
         LOGGER.info("Expected answers of task " + taskId + " sent to Evaluation Storage.");

		} 
		catch (Exception e) {
			LOGGER.error("Exception caught while reading the tasks and their expected answers", e);
		}
		
	}
	
	

	@Override
 public void receiveCommand(byte command, byte[] data) {
    if (Commands.DATA_GENERATION_FINISHED == command) {
 	   inputFilesReceiver.terminate();
    }
    super.receiveCommand(command, data);
 }

 @Override
 public void close() throws IOException {
     LOGGER.info("Closign Task Generator...");

     super.close();
     LOGGER.info("Task Genererator closed successfully.");
 }


}