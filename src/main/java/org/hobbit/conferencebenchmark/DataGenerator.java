package org.hobbit.conferencebenchmark;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.hobbit.core.components.AbstractDataGenerator;
import org.hobbit.core.rabbit.RabbitMQUtils;
import org.hobbit.core.rabbit.SimpleFileSender;
import org.hobbit.conferencebenchmark.task.Task;
import org.hobbit.conferencebenchmark.util.PlatformConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Re-used from the LargeBio track
 *
 * @author ernesto
 * @author ondrej
 * 
 * Modifications by ondrej on 14 Jan 2018
 *
 */

public class DataGenerator extends AbstractDataGenerator {
	private static final Logger LOGGER = LoggerFactory.getLogger(DataGenerator.class);
	
	protected String conference_task;
	
	protected File source_file;
	
	protected File target_file;
	
	protected File reference_file;
	
	private int taskId = 0;
	
	private String task_queue_Name = "conference_task";
	
	private Task task;
	
	private SimpleFileSender sender;
	
	 @Override
	 public void init() throws Exception {
	      LOGGER.info("Initializing Data Generator '" + getGeneratorId() + "'");
	      super.init();

	      //Get Env variables
	      getEnvVariables();

	      
	      task = new Task(Integer.toString(taskId++));

	    }

	
	
	@Override
	/**
	 * Sets the source and target ontologies, and the reference alignment
	 */
	protected void generateData() throws Exception {
		
		
		LOGGER.info("Generate data.. ");
        sender = null;
        
        try {

			prepareTaskDataset();
			
			
			//Workflow. In HOBBIT the workflow and format of task is rather generic and perhaps more oriented to query answering.
			//System get a source dataset (e.g. source ontology) and a Task that will contain the target dataset 
			//(e.g. target ontology) and expected results (e.g. reference alignment). 
			//TODO For OAEI we will keep this worflow for now. But I have also extended the task definition to include not only the target but also the source.
			//TODO For OAEI tracks with multiple tasks (e.g. Conference and Multifarm) this will be useful. The source dataset can just be a "dummy dataset".
				
			
	        //To be set to the Task. Task will also include parameter like matching only classes...
	        prepareReference();
		        
		        
	        //To be sent to the system
	        //prepareSource();
	        prepareQueueNames();
	        
	        
	        //To be sent to the TaskGenerator
	        prepareTasks();
		
        } 
        catch (Exception e) {
            LOGGER.error("Exception while sending file to System Adapter or Task Generator(s).", e);
        }
		
	}
	
	
	/**
	 * 
	 * Sets a Task and sends it to TaskGenerator (target file path and relevant parameters). Dataset itself is sent via the FileSender 
	 * 
	 * @throws IOException 
	 * 
	 */
	private void prepareTasks() throws IOException {
		
		byte[][] generatedFileArray = new byte[3][];
        // send the file name and its content
		//We send both source and target paths: not necessary for largebio but useful for multfarm or conference
        generatedFileArray[0] = RabbitMQUtils.writeString(PlatformConstants.RDFXML);        
        generatedFileArray[1] = RabbitMQUtils.writeString(source_file.getAbsolutePath());//source
        generatedFileArray[2] = RabbitMQUtils.writeString(target_file.getAbsolutePath()); //target
        
        // convert them to byte[]
        byte[] generatedFile = RabbitMQUtils.writeByteArrays(generatedFileArray);
        task.setSourceTarget(generatedFile);
        
        //Set other parameters for the task. i.e. what to match, potentially which URLs, etc.
        //In Largebio we only match classes
        task.setMatchingClassesRequired(true);
        task.setMatchingObjectPropertiesRequired(true);
        task.setMatchingDataPropertiesRequired(true);
        task.setMatchingInstancesRequired(false);
        
        //Task name is the queue name of the task generator to the system
        task.setTaskQueueName(task_queue_Name);
        
        byte[] data = SerializationUtils.serialize(task);

        
        
        // define a queue name, e.g., read it from the environment
        String queueName = "input_files";
        
        // create the sender
        sender = SimpleFileSender.create(this.outgoingDataQueuefactory, queueName);

        InputStream is_source = null;
        InputStream is_target = null;
        try {
            // create input stream, e.g., by opening a file
            is_source = new FileInputStream(source_file);
            is_target = new FileInputStream(target_file);
            // send data
            sender.streamData(is_source, source_file.getName());
            sender.streamData(is_target, target_file.getName());
            
            
        } catch (Exception e) {
            // handle exception
        } finally {
            IOUtils.closeQuietly(is_source);
            IOUtils.closeQuietly(is_target);
        }

        // close the sender
        IOUtils.closeQuietly(sender);

        sendDataToTaskGenerator(data);
        LOGGER.info("Target data successfully sent to Task Generator.");
		
	}

	/**
	 * 
	 * We prepare the queue names where the files for the benchmark tasks will be sent
	 * This information will be treated in the method "receiveGeneratedData" of the SystemAdapter
	 * Single task benchmark will send only one queue name, while multiple task benchmarks like Conference and Multifarm will send many 
	 *  
	 * @throws IOException 
	 * 
	 */
	private void prepareQueueNames() throws IOException {
	
		byte[][] generatedFileArray = new byte[2][];
		
		// send format of the files (inherited from hobbit tasks)
		generatedFileArray[0] = RabbitMQUtils.writeString(PlatformConstants.RDFXML);
		
		//We send queue names to system to initialize set of threads to deal with the received files from Taskgenerator
		//Send as many queues as tasks (execute din one go) in the benchmark
		generatedFileArray[1] = RabbitMQUtils.writeString(task_queue_Name);
		
		
		// convert them to byte[]
		byte[] generatedFile = RabbitMQUtils.writeByteArrays(generatedFileArray);
		
		
		// send data to system
		sendDataToSystemAdapter(generatedFile);
		LOGGER.info(source_file.getAbsolutePath() + " (" + (double) source_file.length() / 1000 + " KB) sent to System Adapter.");
		
	}


	/**
	 * We sent the source dataset (path) to the System adapter. Dataset itself is sent via the FileSender 
	 * @throws IOException 
	 * deprecated 
	 */
	private void prepareSource() throws IOException {

		//TODO data is sent by two means. It seems that currently only the sender is used for the dataset itself (for some size restrictions).		
		//The one that will appear as parameter e.g. byte[] data in the System Adapter or Task Generator(s) only
		//contains the format and the path to the dataset (optionally, it may also contain some relevant parameters)
		
		byte[][] generatedFileArray = new byte[2][];
        
		// send the file name and its content.
        generatedFileArray[0] = RabbitMQUtils.writeString(PlatformConstants.RDFXML);
        generatedFileArray[1] = RabbitMQUtils.writeString(source_file.getAbsolutePath());
        
        // convert them to byte[]
        byte[] generatedFile = RabbitMQUtils.writeByteArrays(generatedFileArray);

        
        //TODO We do not need to send anything as source
        // define a queue name, e.g., read it from the environment
        /*String queueName = "source_file";

        // create the senderfile to System Adapter or Task Generator(s).
        sender = SimpleFileSender.create(this.outgoingDataQueuefactory, queueName);

        InputStream is = null;
        try {
            // create input stream, e.g., by opening a file
            is = new FileInputStream(source_file);
            // send data
            sender.streamData(is, source_file.getName());
        } catch (Exception e) {
            // handle exception
        } finally {
            IOUtils.closeQuietly(is);
        }

        // close the sender
        IOUtils.closeQuietly(sender);
        */
        
        // send data to system
        sendDataToSystemAdapter(generatedFile);
        LOGGER.info(source_file.getAbsolutePath() + " (" + (double) source_file.length() / 1000 + " KB) sent to System Adapter.");
		
	}



	/**
	 * @throws IOException 
	 * 
	 */
	private void prepareReference() throws IOException {
		
		 byte[][] generatedFileArray = new byte[3][];
         // send the file name and its content
         generatedFileArray[0] = RabbitMQUtils.writeString(PlatformConstants.RDFXML);
         generatedFileArray[1] = RabbitMQUtils.writeString(reference_file.getAbsolutePath());
         generatedFileArray[2] = FileUtils.readFileToByteArray(reference_file);
         // convert them to byte[]
         byte[] generatedFile = RabbitMQUtils.writeByteArrays(generatedFileArray);
         
         
         task.setExpectedAnswers(generatedFile);
		
	}

	/**
	 * 
	 */
	private void prepareTaskDataset() {
		
		
		String ontology1;
		String ontology2;
		String reference;
		
		
		
		switch (conference_task) {
		case PlatformConstants.TASK_CMT_CONFERENCE:
        case PlatformConstants.TASK_CMT_CONFERENCE_URI:
        	ontology1="cmt.owl";
        	ontology2="Conference.owl";
        	reference="reference-cmt-conference.rdf";
        	task_queue_Name = "cmt-conference";
        	break;
        
        case PlatformConstants.TASK_CMT_CONFOF:
        case PlatformConstants.TASK_CMT_CONFOF_URI:
        	ontology1="cmt.owl";
        	ontology2="confOf.owl";
        	reference="reference-cmt-confof.rdf";
        	task_queue_Name = "cmt-confof";
        	break;
        
        case PlatformConstants.TASK_CMT_EKAW:
        case PlatformConstants.TASK_CMT_EKAW_URI:
        	ontology1="cmt.owl";
        	ontology2="ekaw.owl";
        	reference="reference-cmt-ekaw.rdf";
        	task_queue_Name = "cmt-ekaw";
        	break;
        	
        case PlatformConstants.TASK_CMT_EDAS:
        case PlatformConstants.TASK_CMT_EDAS_URI:
        	ontology1="cmt.owl";
        	ontology2="edas.owl";
        	reference="reference-cmt-edas.rdf";
        	task_queue_Name = "cmt-edas";
        	break;
        
        case PlatformConstants.TASK_CMT_IASTED:
        case PlatformConstants.TASK_CMT_IASTED_URI:
        	ontology1="cmt.owl";
        	ontology2="iasted.owl";
        	reference="reference-cmt-iasted.rdf";
        	task_queue_Name = "cmt-iasted";
        	break;
        	
        case PlatformConstants.TASK_CMT_SIGKDD:
        case PlatformConstants.TASK_CMT_SIGKDD_URI:
        	ontology1="cmt.owl";
        	ontology2="sigkdd.owl";
        	reference="reference-cmt-sigkdd.rdf";
        	task_queue_Name = "cmt-sigkdd";
        	break;
        
        case PlatformConstants.TASK_CONFERENCE_CONFOF:
        case PlatformConstants.TASK_CONFERENCE_CONFOF_URI:
        	ontology1="Conference.owl";
        	ontology2="confOf.owl";
        	reference="reference-conference-confof.rdf";
        	task_queue_Name = "conference-confof";
        	break;
        
        case PlatformConstants.TASK_CONFERENCE_EDAS:
        case PlatformConstants.TASK_CONFERENCE_EDAS_URI:
        	ontology1="Conference.owl";
        	ontology2="edas.owl";
        	reference="reference-conference-edas.rdf";
        	task_queue_Name = "conference-edas";
        	break;
        
        case PlatformConstants.TASK_CONFERENCE_EKAW:
        case PlatformConstants.TASK_CONFERENCE_EKAW_URI:
        	ontology1="Conference.owl";
        	ontology2="ekaw.owl";
        	reference="reference-conference-ekaw.rdf";
        	task_queue_Name = "conference-ekaw";
        	break;
        
        case PlatformConstants.TASK_CONFERENCE_IASTED:
        case PlatformConstants.TASK_CONFERENCE_IASTED_URI:
        	ontology1="Conference.owl";
        	ontology2="iasted.owl";
        	reference="reference-conference-iasted.rdf";
        	task_queue_Name = "conference-iasted";
        	break;
        
        case PlatformConstants.TASK_CONFERENCE_SIGKDD:
        case PlatformConstants.TASK_CONFERENCE_SIGKDD_URI:
        	ontology1="Conference.owl";
        	ontology2="sigkdd.owl";
        	reference="reference-conference-sigkdd.rdf";
        	task_queue_Name = "conference-sigkdd";
        	break;
        
        case PlatformConstants.TASK_CONFOF_EDAS:
        case PlatformConstants.TASK_CONFOF_EDAS_URI:
        	ontology1="confOf.owl";
        	ontology2="edas.owl";
        	reference="reference-confof-edas.rdf";
        	task_queue_Name = "confof-edas";
        	break;

        case PlatformConstants.TASK_CONFOF_EKAW:
        case PlatformConstants.TASK_CONFOF_EKAW_URI:
        	ontology1="confOf.owl";
        	ontology2="ekaw.owl";
        	reference="reference-confof-ekaw.rdf";
        	task_queue_Name = "confof-ekaw";
        	break;
        
        case PlatformConstants.TASK_CONFOF_IASTED:
        case PlatformConstants.TASK_CONFOF_IASTED_URI:
        	ontology1="confOf.owl";
        	ontology2="iasted.owl";
        	reference="reference-confof-iasted.rdf";
        	task_queue_Name = "confof-iasted";
        	break;
        
        case PlatformConstants.TASK_CONFOF_SIGKDD:
        case PlatformConstants.TASK_CONFOF_SIGKDD_URI:
        	ontology1="confOf.owl";
        	ontology2="sigkdd.owl";
        	reference="reference-confof-sigkdd.rdf";
        	task_queue_Name = "confof-sigkdd";
        	break;
        
        case PlatformConstants.TASK_EDAS_EKAW:
        case PlatformConstants.TASK_EDAS_EKAW_URI:
        	ontology1="edas.owl";
        	ontology2="ekaw.owl";
        	reference="reference-edas-ekaw.rdf";
        	task_queue_Name = "edas-ekaw";
        	break;
        
        case PlatformConstants.TASK_EDAS_IASTED:
        case PlatformConstants.TASK_EDAS_IASTED_URI:
        	ontology1="edas.owl";
        	ontology2="iasted.owl";
        	reference="reference-edas-iasted.rdf";
        	task_queue_Name = "edas-iasted";
        	break;
        
        case PlatformConstants.TASK_EDAS_SIGKDD:
        case PlatformConstants.TASK_EDAS_SIGKDD_URI:
        	ontology1="edas.owl";
        	ontology2="sigkdd.owl";
        	reference="reference-edas-sigkdd.rdf";
        	task_queue_Name = "edas-sigkdd";
        	break;
        
        case PlatformConstants.TASK_EKAW_IASTED:
        case PlatformConstants.TASK_EKAW_IASTED_URI:
        	ontology1="ekaw.owl";
        	ontology2="iasted.owl";
        	reference="reference-ekaw-iasted.rdf";
        	task_queue_Name = "ekaw-iasted";
        	break;
        
        case PlatformConstants.TASK_EKAW_SIGKDD:
        case PlatformConstants.TASK_EKAW_SIGKDD_URI:
        	ontology1="ekaw.owl";
        	ontology2="sigkdd.owl";
        	reference="reference-ekaw-sigkdd.rdf";
        	task_queue_Name = "ekaw-sigkdd";
        	break;
        
        case PlatformConstants.TASK_IASTED_SIGKDD:
        case PlatformConstants.TASK_IASTED_SIGKDD_URI:
        	ontology1="iasted.owl";
        	ontology2="sigkdd.owl";
        	reference="reference-iasted-sigkdd.rdf";
        	task_queue_Name = "iasted-sigkdd";
        	break;
        	
        default:
        	ontology1="cmt.owl";
        	ontology2="ekaw.owl";
        	reference="reference-cmt-ekaw.rdf";
        	task_queue_Name = "cmt-ekaw";
        	break;
		}
		
        source_file = new File(PlatformConstants.datasetsPath + File.separator + ontology1);

        target_file = new File(PlatformConstants.datasetsPath + File.separator + ontology2);
        
        reference_file = new File(PlatformConstants.datasetsPath + File.separator + reference);
        
        
	}


	/**
	 * The BenchmarkGenerator tell the task to be executed
	 */
	public void getEnvVariables() {
        LOGGER.info("Getting Data Generator's properites from the environment...");

        Map<String, String> env = System.getenv();
        
        //Given by BenchmarkGenerator and defined in benchmark.ttl file
        conference_task = (String) getFromEnv(env, PlatformConstants.TASK, "");
        
    }


	
	
    /**
     * A generic method for initialize benchmark parameters from environment
     * variables
     *
     * @param env a map of all available environment variables
     * @param parameter the property that we want to get
     * @param paramType a dummy parameter to recognize property's type
     */
    @SuppressWarnings("unchecked")
    private <T> T getFromEnv(Map<String, String> env, String parameter, T paramType) {
        if (!env.containsKey(parameter)) {
            LOGGER.error(
                    "Environment variable \"" + parameter + "\" is not set. Aborting.");
            throw new IllegalArgumentException(
                    "Environment variable \"" + parameter + "\" is not set. Aborting.");
        }
        try {
            if (paramType instanceof String) {
                return (T) env.get(parameter);
            } else if (paramType instanceof Integer) {
                return (T) (Integer) Integer.parseInt(env.get(parameter));
            } else if (paramType instanceof Long) {
                return (T) (Long) Long.parseLong(env.get(parameter));
            } else if (paramType instanceof Double) {
                return (T) (Double) Double.parseDouble(env.get(parameter));
            }
        } catch (Exception e) {
            throw new IllegalArgumentException(
                    "Couldn't get \"" + parameter + "\" from the environment. Aborting.", e);
        }
        return paramType;
    }

}
